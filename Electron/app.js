'use strict';

const async = require('async');
const fs = require('fs');
const osenv = require('osenv');
const path = require('path');

function hentHjemmemappe() {
  return osenv.home();
}

function hentFraMappe(sti, cb) {
  fs.readdir(sti, cb);
}

function hentFiltype(filePath, cb) {
  let result = {
    fil: path.basename(filePath),
    path: filePath, 
    type: ''
  };
  fs.stat(filePath, (err, stat) => {
    if (err) {
      cb(err);
    } 
    else {
      if (stat.isFile()) {
        result.type = 'fil';
    	}
      if (stat.isDirectory()) {
        result.type = 'mappe';
      }
      cb(err, result);
    }
  });
}

function hentFiltyper(sti, filer, cb) {
  async.map(filer, (file, asyncCb) => {
    let resolvedFilePath = path.resolve(sti, file);
    hentFiltype(resolvedFilePath, asyncCb);
  }, cb);
}

function visFil(file) {
  const mainArea = document.getElementById('main-area');
  const template = document.querySelector('#item-template');
  let clone = document.importNode(template.content, true);
  clone.querySelector('img').src = `images/${file.type}.svg`;
  clone.querySelector('.filename').innerText = file.file;
  mainArea.appendChild(clone);
}

function visFiler(err, filer) {
  if (err) {
    return alert('Shit! Kunne ikke hente filene.');
  }
  filer.forEach(visFil);
}

function main() {
  let sti = hentHjemmemappe();
  hentFraMappe(sti, (err, filer) => {
    if (err) {
      return alert('Fuck! Hjemmemappe kunne ikke hentes.');
    }
    hentFiltyper(sti, filer, visFiler);
  });
}

main();
