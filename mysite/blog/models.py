from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from taggit.managers import TaggableManager
from haystack.query import SearchQuerySet
# Create your models here.



class PublishedManager(models.Manager):
	def get_query_set(self):
		return (super(PublishedManager, self).
			get_queryset().filter(status='published'))



class Post(models.Model):
	STATUS_CHOICES = (
		('draft', 'Draft'),
		('published', 'Published'),
	)

	title = models.CharField(max_length=250)
	slug = models.SlugField(max_length=250, unique_for_date='publish')
	author = models.ForeignKey(User, related_name="blog_post")
	body = models.TextField()
	publish = models.DateTimeField(default=timezone.now)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.CharField(max_length=10, choices=STATUS_CHOICES,
		default="draft")
	status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft')
	objects = models.Manager() # Default manager
	published = PublishedManager() # Customer manager
	tags = TaggableManager() # Tag manager


	class Meta:
		ordering = ('-publish',)

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('post_detail',
			args=[self.publish.year,
			self.publish.strftime('%m'),
			self.publish.strftime('%d'),
			self.slug])


class Comment(models.Model):
	post = models.ForeignKey(Post, related_name='comments')
	name = models.CharField(max_length=180)
	email = models.EmailField()
	body = models.TextField(default='')
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	active = models.BooleanField(default=True)

	class Meta:
		ordering = ('created',)

	def __str__(self):
		return 'Comment by {} on {}'.format(self.name, self.post)


def post_search(request):
	form = SearchForm()
	if 'query' in request.GET:
		form = SearchForm(request.GET)
		if form.is_valid():
			cd = form.cleaned_data
			results = SearchQuerySet().\
				models(Post).filter(content=cd['query']).load_all()
			total_results = results.count()
	return render(request,
		'blog/post/search.html',
		{'form': form, 'cd': cd, 
		'results': results, 'total_results': total_results})