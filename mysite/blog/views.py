from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView
from .models import Post, Comment
from .forms import EmailPostForm, CommentForm, SearchForm
from django.core.mail import send_mail
from django.db.models import Count
from taggit.models import Tag
from haystack.query import SearchQuerySet

# Create your views here.

class PostListView(ListView):
	queryset = Post.published.all()
	context_object_name = 'posts'
	paginate_by = 3
	template_name = 'blog/post/list.html'


def post_list(request, tag_slug=None):
	object_list = Post.published.all()
	tag = None
	if tag_slug:
		tag = get_object_or_404(Tag, slug=tag_slug)
		object_list = object_list.filter(tags__in=[tag])

	paginator = Paginator(object_list, 3)
	page = request.GET.get('page')
	items = request.GET.items()
	
	print(page)

	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		print("Page is not an integer")
		posts = paginator.page(1)

	except EmptyPage:
		print("Page is empty")
		posts = paginator.page(paginator.num_pages)

	print("Now returning 'blog/post/list.html'")
	
	return render(request, 
		'blog/post/list.html', 
		{'posts': posts, 'page': page, 'tag': tag})


def get_tag(object_list, tag_slug=None):
	if tag_slug:
		tag = get_object_or_404(Tag, slug=tag_slug)
		return object_list.filter(tags__in=[tag])


def post_detail(request, year, month, day, post):
	post = get_object_or_404(
		Post, slug=post, status="draft", 
		publish__year=year, publish__month=month, publish__day=day)

	comments = post.comments.filter(active=True)

	# Hvis et innlegg har blitt lagt inn
	if request.method == 'POST':
		comment_form = CommentForm(data=request.POST)

		# Opprett et nytt comment-objekt og la det "peke" paa
		# post
		if comment_form.is_valid():
			new_comment = comment_form.save(commit=False)
			new_comment.post = post
	else:
		comment_form = CommentForm()

	post_tags_ids = post.tags.values_list('id', flat=True)
	similar_posts = Post.published.filter(tags__in=post_tags_ids)\
		.exclude(id=post.id)
	similar_posts = similar_posts.annotate(same_tags=Count('tags'))\
		.order_by('-same_tags', '-publish')[:4]

	return render(request, 
		'blog/post/detail.html', 
		{
		'post': post, 'comments': comments, 
		'comment_form': comment_form, 'similar_posts': similar_posts
		})


def post_share(request, post_id):
	post = get_object_or_404(Post, id=post_id, status="draft")
	sent = False
	print("Inside post share")
	if request.method == 'POST':
		form = EmailPostForm(request.POST)

		if form.is_valid():
			cd = form.cleaned_data
			post_url = request.build_absolute_uri(
				post.get_absolute_url())
			print(cd['comments'])
			
			subject = '{} ({}) recommends you reading "{}"'.format(
				cd['name'], cd['email'], post.title)
			
			message = 'Read "{}" at {}\n\n\'s comments: {}'.format(
				post.title, post_url, cd['name'], cd['comments'])

			send_mail(subject, message, 'hardtliv@gmail.com', [cd['to']])

			sent = True
			return render(request, 
					'blog/post/share.html',
					{'post': post, 'form': form, 'sent': sent, 'cd': cd})
	else:
		form = EmailPostForm()
		print("Going to blog/post/share.html without arguments")
		return render(request, 
					'blog/post/share.html',
					{'post': post, 'form': form, 'sent': False, 'cd': None})





def post_search(request):
	form = SearchForm()

	if 'query' in request.GET:
		form = SearchForm(request.GET)
		if form.is_valid():
			cd = form.cleaned_data
			results = SearchQuerySet().models(Post)\
				.filter(content=cd['query']).load_all()

			total_results = results.count()

			return render(request, 'blog/post/search.html',
				{'form': form, 'cd': cd, 
			 	'results': results, 'total_results': total_results})

	return render(request, 'blog/post/search.html',
			{'form': form, 'cd': None, 
			 'results': None, 'total_results': None})