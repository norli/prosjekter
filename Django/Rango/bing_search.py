import json
import requests

import http.client, urllib.request, urllib.parse, urllib.error, base64

def read_bing_key():
	bing_api_key = None

	try:
		with open('bing.key', 'r') as f:
			bing_api_key = f.readline()
	except:
		raise IOError('bing.key file not found')
	return bing_api_key


def run_query(search_terms):
	bing_api_key = read_bing_key()

	if not bing_api_key:
		raise KeyError("Bing Key Not Found")

	root_url ="https://api.cognitive.microsoft.com/bing/v5.0/search"
	params = urllib.parse.urlencode({
    # Request parameters
    'q': search_terms,
    'count': '10',
    'offset': '0',
    'mkt': 'en-us',
    'safesearch': 'Off',
	})
	
	headers = {'Ocp-Apim-Subscription-Key': bing_api_key}
	
	try:
		conn = http.client.HTTPSConnection('api.cognitive.microsoft.com')
		conn.request("GET", "/bing/v5.0/search?%s" % params, "{body}", headers)
		response = conn.getresponse()
		data = response.read()
		conn.close()

		return data
	except Exception as e:
		print("[Errno {0}] {1}".format(e.errno, e.strerror))

	print('*************** Outer ******************')


def main():
	query = input("Enter search query: ")
	result = run_query(query)
	result = str(result,'utf-8')

	json_res = json.loads(result)
	for page in json_res['webPages']['value']:
		print(json_res['webPages']['webSearchUrl'])
		print(page['name'])
		print('*************** Outer ******************')


if __name__ == '__main__':
	main()